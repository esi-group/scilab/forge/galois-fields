
GALOIS FIELDS TOOLBOX v0.1
-----------------------------

Developed by Daniel Rodriguez, with the help of Msc Rafael Isaacs (Universidad Industrial de Santander, Colombia).

Any suggestions or issues with this toolbox, feel free to send an email to danrod@gmail.com

WARNING: This toolbox is still in early stages of development, the algorithms for checking if a primitive polynomial is primitive and printing the list of primitive polynomials are very slow for
greater values of p and n (the algorithms are based on brute force techniques), and the primality test is based on stochastic methods, as opposed to deterministic algorithms, which are faster.

Description
----------

This module allows any user to work with Galois Fields (also called Finite Sields) in Scilab. A Galois field is a field with a finite number of elements, and it has applications in error 
control codes and in cryptographic algorithms like AES and RSA. The list of current features available in this module are the following:

* primality test
* Creation of prime fields and extension fields, given p characteristic, degree n, and the primitive polynomial.
* Extraction of information from finite fields: order, degree, primitive polynomial and the elements of the field.
* Listing of the primitive elements of a finite field (both prime and extension fields).
* Checking if a polynomial is primitive or not.
* List all the primitive polynomials given p and n.
* Find the default primitive polynomial for the combination of p and n.
* Find the minimal polynomial of an element from a prime field.
* Print addition, substraction, multiplication and division tables for prime fields.
* Perform arithmetic operations (addition, substraction, multiplication, division and exponentiation) between elements of finite fields (both prime and extension fields)
* Perform polynomial operations (addition, substraction, multiplication, division and exponentiation) between polynomials defined on prime fields.
* Prints the elements of a finite field in polynomial, exponential and integer format.
* 

Contents of the toolbox
-----------------------

Requirements
-------------

* Windows XP or greater (tested on Windows XP mostly)
* Scilab 5.1.x or greater
* 

Installation
----------

Copyright
---------




